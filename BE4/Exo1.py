import pyb


# Déclaration : 

bouton_vert = pyb.Pin("X1")
bouton_rouge = pyb.Pin("X2")
led = pyb.Pin("X3")
bouton_vert.init(pyb.Pin.IN, pyb.Pin.PULL_UP)
bouton_rouge.init(pyb.Pin.IN, pyb.Pin.PULL_UP)
led.init(pyb.Pin.OUT_PP)
timer = pyb.Timer(4)

# Allumer et éteindre la LED avec deux bouttons : 
def btn_r_btn_v():
    flag = False
    while True:
        if bouton_vert.value() == 0 and not flag :
            led.high()
            flag = True
        elif bouton_rouge.value() == 0 and flag : 
            led.low()
            flag = False

# Allumer la LED avec les interruptions : 
def interruption_vert(line):
    print("LED")
    timer.deinit()
    led.high()
    

def interruption_rouge(line):
    led.low()
            
def interruption():
    while(True):   
        irq_vert = pyb.ExtInt(bouton_vert, pyb.ExtInt.IRQ_FALLING,pyb.Pin.PULL_UP, interruption_vert)
        irq_rouge = pyb.ExtInt(bouton_rouge, pyb.ExtInt.IRQ_FALLING,pyb.Pin.PULL_UP, interruption_rouge)

# Faire clignotter la LED : 

def clignotter_sans_interruption():
    while(True):
        led.high()
        pyb.delay(500)
        led.low()
        pyb.delay(500)

def timer_callback(line) :
    led.value(not led.value())

def clignotter_avec_interruption(line):
    #timer = pyb.Timer(4)
    timer.callback(timer_callback)
    timer.init(freq = 1)
    #timer.dinit()


irq_vert = pyb.ExtInt(bouton_vert, pyb.ExtInt.IRQ_FALLING,pyb.Pin.PULL_UP, clignotter_avec_interruption)
irq_rouge = pyb.ExtInt(bouton_rouge, pyb.ExtInt.IRQ_FALLING,pyb.Pin.PULL_UP, interruption_vert)

              


    



    