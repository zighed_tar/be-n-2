#Codage de fonctions utilitaires

def matrixOn(on):
    if(on):
        serialWrite(0x0C, 1)
    else:
        serialWrite(0x0C, 0)

def matrixTest(test):
    if(test):
        serialWrite(0x0F, 1)
    else:
        serialWrite(0x0F, 0)

def matrixIntensity(percent):
    if(percent < 25):
        serialWrite(0x0A, 0)

    elif(percent < 50):
        serialWrite(0x0A, 1)

    elif(percent < 75):
        serialWrite(0x0A, 2)
        
    else:
        serialWrite(0x0A, 3)

def matrixDecode(decode):
    if(decode):
        serialWrite(0x09, 1)
    else:
        serialWrite(0x09, 0)

def matrixDigits(num):
    serialWrite(0x0B, 7) # au cas ou reset
    for i in range(9):
        serialWrite(i, 0)

    for j in range(1, num + 2):
        serialWrite(j, 0xFF)

def matrixLine(num, value):
    if((num >= 0 and num <= 7) and (value > 0 and value <= 0xFF)):
        serialWrite(num + 1, value)