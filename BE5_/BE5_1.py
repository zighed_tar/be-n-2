#
# Practical session #5: Drive a 8x8 LED matrix using a MAX7219
#
# frederic.boulanger@centralesupelec.fr
# 2020-11-08
# copyright CentraleSupÃ©lec
#
import pyb
 
data = "X8";    # patte connectÃ©e Ã  l'entrÃ©e sÃ©rie du MAX7219 (DIN)
load = "X5";    # patte de chargement des donnÃ©es (CS)
clk  = "X6";    # patte donnant l'horloge de la liaison sÃ©rie (CLK)
 
# Initialisation des pattes en sortie en mode push-pull
dataPin = pyb.Pin(data, pyb.Pin.OUT_PP)
loadPin = pyb.Pin(load, pyb.Pin.OUT_PP)
clkPin  = pyb.Pin(clk, pyb.Pin.OUT_PP)
 
# Mise Ã  zÃ©ro des pattes
dataPin.low()
loadPin.low()
clkPin.low()
 
# Transmet un octet bit par bit vers le MAX7219, bit de poids fort en premier
def serialShiftByte(data):
    # Mise Ã  zÃ©ro du signal d'horloge pour pouvoir faire un front montant plus tard
    clkPin.low()
    # DÃ©calage des 8 bits de donnÃ©es
    for i in range(8):
        # on dÃ©cale la donnÃ©e de i bits vers la gauche et on teste le bit de poids fort
        value = ((data << i) & 0B10000000) != 0
        dataPin.value(value)  # on met la patte DIN Ã  cette valeur
        clkPin.high()         # puis on crÃ©e une impulsion sur CLK
        clkPin.low()          # pour transmettre ce bit
 
# Ã‰criture d'une donnÃ©e dans un registre du MAX7219.
def serialWrite(address, data):
    # Mise Ã  zÃ©ro du signal CS pour pouvoir crÃ©er un front montant plus tard
    loadPin.low()
    # On envoie l'adresse en premier
    serialShiftByte(address)
    # puis la donnÃ©e
    serialShiftByte(data)    # et on crÃ©e une impulsion sur la ligne CS pour charger la donnÃ©e dans le registre
    loadPin.high()
    loadPin.low()