# Codage contrôle des pixels

def updateDisplay(bitmap):
    for i in range(8):
        serialWrite(i + 1, bitmap[i])

def clearDisplay(bitmap):
    for i in bitmap:
        i = 0
    updateDisplay(bitmap)

def setPixel(x, y, on, bitmap):
    if on:
        bitmap[x] = bitmap[x] | 2**y
    else:
        bitmap[x] = bitmap[x] & ~ (2**y)

def getPixel(x, y, bitmap):
    if (bitmap[x] & 2**y):
        return True
    else:
        return False
