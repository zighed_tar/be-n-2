# Code affichage de l'image 

def displayPict(pict):
    if (len(pict) != 8 or sum(len(i) != 8 for i in pict)):
        print("Size error")
        return

    bitmap = bytearray(8)
    matrixOn(1)
    matrixTest(0)
    matrixDecode(0)
    matrixDigits(7)
    clearDisplay(bitmap)
    
    for y in range(8):
        for x in range(8):
            if pict[y][x] != " ":
                setPixel(x, y, 1, bitmap)
            else:
                setPixel(x, y, 0, bitmap)

    updateDisplay(bitmap)
    
    return bitmap

def testDisplayPict():
    smiley = (
        "  ****  ",
        " *    * ",
        "* *  * *",
        "*      *",
        "* *  * *",
        "*  **  *",
        " *    * ",
        "  ****  "
    )
    displayPict(smiley)

def testDisplayPict2():
    frowney = (
        "  ****  ",
        " *    * ",
        "* *  * *",
        "*      *",
        "*  **  *",
        "* *  * *",
        " *    * ",
        "  ****  "
    )
    displayPict(frowney)