# code le jeu de la vie

import os
def randomBitmap():
    bitmap = bytearray(8)
    for y in range(8):
        for x in range(8):
            if os.urandom(1)[0] > 127 :
                setPixel(x, y, 1, bitmap)
            else:
                setPixel(x, y, 0, bitmap)
    return bitmap


def countNeighbours(x, y, bitmap):
    neighbs = 0
    coord = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1), (x + 1, y + 1),(x + 1, y - 1), (x - 1, y + 1),  (x - 1, y - 1)]
    for i in coord:
        if getPixel(i[0] % 8, i[1] % 8, bitmap):
            neighbs += 1

    return neighbs

def lifeStep(bitmap):
    new_bitmap = bytearray(8)
    for y in range(8):
        for x in range(8):
            n = countNeighbours(x, y, bitmap)
            if n == 3:
                setPixel(x, y, 1, new_bitmap)
            elif n == 2:
                setPixel(x, y, int(getPixel(x, y, bitmap)), new_bitmap)
            else:
                setPixel(x, y, 0, new_bitmap)
    
    for i in range(len(bitmap)):
        bitmap[i] = new_bitmap[i]

    updateDisplay(bitmap)

def gameOfLife():
    bitmap = randomBitmap()
    matrixOn(1)
    matrixTest(0)
    matrixDecode(0)
    matrixDigits(7)
    clearDisplay(bitmap)

    while(1):
        pyb.delay(500)
        lifeStep(bitmap)

        #test pour le jeu de la vie
# Figures stables
stableBlock = (
  "        ",
  "        ",
  "        ",
  "   **   ",
  "   **   ",
  "        ",
  "        ",
  "        "
)
def testBlock():
    bitmap = displayPict(stableBlock)
    pyb.delay(500)
    lifeStep(bitmap)
 
stableTube = (
  "        ",
  "        ",
  "   *    ",
  "  * *   ",
  "   *    ",
  "        ",
  "        ",
  "        "
)
def testTube():
    bitmap = displayPict(stableTube)
    pyb.delay(500)
    lifeStep(bitmap)
 
# Figures oscillantes
oscBlinker = (
  "        ",
  "        ",
  "        ",
  "  ***   ",
  "        ",
  "        ",
  "        ",
  "        "
)
def testBlinker():
    bitmap = displayPict(oscBlinker)
    for i in range(2):
        pyb.delay(500)
        lifeStep(bitmap)
 
# Vaisseaux
shipGlider = (
  "        ",
  "        ",
  "        ",
  "        ",
  "        ",
  "***     ",
  "  *     ",
  " *      "
)
def testGlider():
    bitmap = displayPict(shipGlider)
    for i in range(32):
        pyb.delay(500)
        lifeStep(bitmap)
 
shipLWSS = (
  "        ",
  "        ",
  "*  *    ",
  "    *   ",
  "*   *   ",
  " ****   ",
  "        ",
  "        "
)
def testLWSS():
    bitmap = displayPict(shipLWSS)
    for i in range(16):
        pyb.delay(500)
        lifeStep(bitmap)